// Nama : Faisal Hanafi
// Prodi : D3 Teknik Komputer
// NIM : 131221018

#include <stdio.h>
#include <conio.h>

void karakter()
{
    char c1,c2,c3;
    printf("Masukan 3 karakter : ");
    scanf("%c%c%c",&c1,&c2,&c3);
    printf("\n");
    printf("karakter yang kamu masukan : %c %c %c \n",c1,c2,c3);
}


void kataKata()
{
    char kata[50];

    printf("Masukan kata : ");
    scanf("%s",kata);
    printf("\n");
    printf("kata yang kamu masukan : %s \n",kata);
}

void angkaa()
{
    int angka;

    printf("Masukan nilai desimal : ");
    scanf("%d", &angka);
    printf("nilai yang dimasukan %d \n",angka);
    printf("Masukan nilai oktal : ");
    scanf("%o", &angka);
    printf("nilai yang dimasukan %o (desimal %d) \n",angka,angka);
    printf("Masukan nilai hexadesimal : ");
    scanf("%x", &angka);
    printf("nilai yang dimasukan %x (desimal %d) \n",angka,angka);
}


void pecahan()
{
    float f;
    int i;
    printf("Masukan nilai pecahan dan integer : ");
    scanf("%5f %5i", &f,&i);
    printf("nilai pecahan : %5.2f \n", f);
    printf("nilai integer : %5i \n", i);
}

void main()
{
    //angkaa();
    //kataKata();
    // karakter();
    pecahan();
}
