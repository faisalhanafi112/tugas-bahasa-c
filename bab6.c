// Nama : Faisal Hanafi
// Prodi : D3 Teknik Komputer
// NIM : 131221018


#include <stdio.h>


//Tugas 1
void Tugas1() {
  for (int i = 0; i <= 100; i++) {
    printf("Teknik Komputer YES \n");
  }
}


//Tugas 2
void Tugas2() {
  int angka = 5;
  while(angka <= 100) {
    printf("%i \n",angka);
    angka+=5;
  }
}

//Tugas3
void Tugas3() {
  int a,b = 0;
  printf("masukan angka : ");
  scanf("%d",&a);
  for (int i = 1; i <= a; i++) {
    printf("%i \n",i);
    b = b + i;
  }
  printf("hasilnya = %i",b);
}


//Tugas4
void Tugas4() {
  int a,b = 0;
  printf("masukan angka : ");
  scanf("%d",&a);
  if(a >= 0){
      for (int i = 1; i <= a; i++) {
        printf("%i \n",i);
        b = b + i;
      }
      printf("hasilnya = %i",b);
  } else {
      printf("Hanya boleh bilangan positif");
  }
}


//Tugas5
void Tugas5() {
  int a,b = 0;
  float mean;
  printf("masukan angka : ");
  scanf("%d",&a);
  if(a >= 0){
      for (int i = 1; i <= a; i++) {
        printf("%i \n",i);
        b += i;
      }
      
      mean = b / a;
      printf("rata-rata = %.0f \n",mean);
      printf("hasilnya = %i",b);
  } else {
      printf("Hanya boleh bilangan positif");
  }
}

//Pilih Tugas
void pilihanTugas() {
  int pilihan;
  printf("KETERANGAN \n Angka 1 -5 untuk Tugas 1 sampai 5 Bab : 6 \n");
  printf("Masukan pilihan : ");
  scanf("%d",&pilihan);
  if(pilihan == 1) {
      return Tugas1();
  } else if(pilihan == 2) {
      return Tugas2();
  } else if(pilihan == 3) {
      return Tugas3();
  } else if(pilihan == 4) {
      return Tugas4();
  } else if(pilihan == 5) {
      return Tugas5();
  } else {
      printf("Salah Memasukkan Pilihan \n");
  }
}

void main() {
  pilihanTugas();
}