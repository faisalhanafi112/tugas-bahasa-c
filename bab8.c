#include <stdio.h>
#include <string.h>


int tugas1() {
  char huruf[10] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};
  printf("Seluruh elemen dari array huruf: ");
  for (char *p = huruf; p < huruf + 10; p++) {
    printf("%c ", *p);
  }
  printf("\n");

  return 0;
}



int tugas2() {
  int bilangan[6] = {0, 2, 4, 6, 8, 10};
  printf("Seluruh elemen dari array bilangan: ");
  for (int *p = bilangan; p < bilangan + 6; p++) {
    printf("%d ", *p);
  }
  printf("\n");

  return 0;
}



int tugas3() {
  int angka[5];
  printf("Masukkan 5 angka: ");
  for (int *p = angka; p < angka + 5; p++) {
    scanf("%d", p);
  }

  printf("Seluruh elemen dari array angka: ");
  for (int *p = angka; p < angka + 5; p++) {
    printf("%d ", *p);
  }
  printf("\n");

  return 0;
}



int tugas4() {
  char str[100];
  printf("Masukkan sebuah string: ");
  scanf("%s", str);

  printf("String yang dimasukkan: ");
  for (char *p = str; *p != '\0'; p++) {
    printf("%c ", *p);
  }
  printf("\n");

  return 0;
}




int tugas5() {
  char str[100];
  printf("Masukkan sebuah string: ");
  scanf("%s", str);
  int panjang = 0;
  for (char *p = str; *p != '\0'; p++) {
    panjang++;
  }

  printf("Panjang string: %d\n", panjang);

  return 0;
}


int tugas6() {
  char str1[100] = "Hanafi";
  char str2[100];
  for (char *p1 = str1, *p2 = str2; *p1 != '\0'; p1++, p2++) {
    *p2 = *p1;
  }
  char *p2 = '\0';
  printf("String str2: %s\n", str2);

  return 0;
}



int tugas7() {
  char str1[100] = "Faisal ";
  char str2[100] = "Hanafi";
  char str3[200];
  for (char *p1 = str1, *p2 = str2, *p3 = str3; *p1 != '\0'; p1++, p3++) {
    *p3 = *p1;
  }
  for (char *p2 = str2, *p3 = p3; *p2 != '\0'; p2++, p3++) {
    *p3 = *p2;
  }
    char *p3 = '\0';

   printf("String str3: %s\n", str3);

  return 0;
}



int tugas8() {

  int tahun[4] = {2006, 2007, 2008, 2009};
  int jumlah[4] = {657, 950, 1345,1750};


  printf("Data tahun dan jumlah:\n");
  for (int *p1 = tahun, *p2 = jumlah; p1 < tahun + 4; p1++, p2++) {
    printf("Tahun: %d, Jumlah: %d\n", *p1, *p2);
  }

  return 0;
}


void main() {
   //tugas1();
   //tugas2();
   //tugas3();
   //tugas4();
   //tugas5();
   //tugas6();
   //tugas7();
   //tugas8();

}
