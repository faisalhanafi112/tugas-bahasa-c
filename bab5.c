﻿// Nama : Faisal Hanafi
// Prodi : D3 Teknik Komputer
// NIM : 131221018

#include <stdio.h>
#include <unistd.h>


//Tugas Satu

void tugasSatu() {
  int a,b;
  printf("Soal : \nBuatlah program yang akan menerima 2 buah angka integer dan menentukan bilangan terbesar dari kedua angka tsb.\n");
  sleep(2);
  printf("Jawaban ... \n");
  printf("masukan angka ke 1 : ");
  scanf("%d",&a);
  printf("masukan angka ke 2 : ");
  scanf("%d",&b);
  
  if(a > b) {
    printf("angka %d lebih besar dari angka %d", a, b);
  } else if (a < b) {
    printf("angka %d lebih besar dari angka %d", b, a);
  } else if (a == b) {
    printf("angka keduanya sama %d", a);
  } else {
    printf("angka yang dimasukan tidak sesuai");
  }
}

//Tugas Dua

void tugasDua() {
  int a;
  printf("Soal : \nBuatlah program yang akan menentukan posisi saklar lampu (ON/OFF). Apabila user memasukkan nol berarti kondisi OFF, sedangkan jika memasukkan satu berarti ON.\n");
  sleep(2);
  printf("Jawaban ... \n");
  printf("Keterangan : \n angka 1 untuk ON \n angka 0 untuk OFF \n Masukan : ");
  scanf("%d",&a);
  
  switch(a) {
    case 0:
     printf("Saklar OFF");
    break;
   case 1:
     printf("Saklar ON");
     break;
   default:
     printf("Pilihan salah");
  }
}


//Tugas Tiga

void tugasTiga() {
  double a;
  double b = 650.0;
  printf("Soal : \nBuatlah sebuah program untuk menentukan kondisi pintu air. User akan memasukkan sebuah nilai pecahan yaitu tinggi air (dalam meter). Apabila tinggi air <= 500.0 m, status AMAN, tinggi air antara 500.1-600.0 m status WASPADA, tinggi air antara 600.1-650.0 m status SIAGA 2, tinggi air >650m status SIAGA 1.\n");
  sleep(2);
  printf("Jawaban ...\n");
  printf("masukan ketinggian air  : ");
  scanf("%lf",&a);
  
  if(a <= 500.0 ) {
    printf("ketinggian %.1lf m Status : AMAN", a);
  } else if (a >= 500.1 && a <= 600.0) {
    printf("ketinggian %.1lf m Status : WASPADA", a);
  } else if (a >= 600.1 && a <= b) {
    printf("ketinggian %.1lf m Status : SIAGA 2", a);
  } else if (a > b) {
    printf("ketinggian %.1lf m Status : SIAGA 1", a);
  } else {
    printf("Angka yang dimasukan salah");
  }
}


//Tugas Empat

void tugasEmpat() {
  char a;
  printf("Soal : \nBuatlah program yang akan menerima sebuah huruf dan menentukan apakah termasuk huruf vokal atau konsonan.\n");
  sleep(2);
  printf("\nMasukan Huruf : ");
  scanf("%c", &a);
  
  switch(a) {
    case 'a':
     printf("Huruf Vokal");
    break;
    case 'i':
     printf("Huruf Vokal");
     break;
    case 'u':
     printf("Huruf Vokal");
     break;
    case 'e':
     printf("Huruf Vokal");
     break;
    case 'o':
     printf("Huruf Vokal");
     break;
   default:
     printf("Huruf Konsonan");
  }
}


//Tugas Lima

void tugasLima() {
  int a;
  int b;
  int c;
  printf("Soal : \nBuatlah sebuah program yang akan meminta user memasukkan 3 integer dipisahkan dengan ‘-’, seperti : 1-10-2009. Program kemudian akan mencetak ke layar tulisan “1 Oktober 2009”\n");
  sleep(2);
  printf("Jawaban ...\n");
  printf("Masukan tanggal : ");
  scanf("%d",&a);
  printf("Masukan bulan : ");
  scanf("%d",&b);
  printf("Masukan tahun : ");
  scanf("%d",&c);
  printf("(%d - %d - %d) \ndikonversikan .... \n",a,b,c);
  sleep(5);
  switch(b) {
    case 1:
    printf("%d Januari %d",a,c);
    break;
    case 2:
    printf("%d Februari %d",a,c);
    break;
    case 3:
    printf("%d Maret %d",a,c);
    break;
    case 4:
    printf("%d April %d",a,c);
    break;
    case 5:
    printf("%d Mei %d",a,c);
    break;
    case 6:
    printf("%d Juni %d",a,c);
    break;
    case 7:
    printf("%d Juli %d",a,c);
    break;
    case 8:
    printf("%d Agustus %d",a,c);
    break;
    case 9:
    printf("%d September %d",a,c);
    break;
    case 10:
    printf("%d Oktober %d",a,c);
    break;
    case 11:
    printf("%d November %d",a,c);
    break;
    case 12:
    printf("%d Desember %d",a,c);
    break;
    default:
    printf("Salah");
  }
  
}


//Pilih Tugas
void pilihanTugas() {
  int pilihan;
  printf("KETERANGAN \n Angka 1 -5 untuk Tugas 1 sampai 5 Bab : 5 \n");
  printf("Masukan pilihan : ");
  scanf("%d",&pilihan);
  if(pilihan == 1) {
      return tugasSatu();
  } else if(pilihan == 2) {
      return tugasDua();
  } else if(pilihan == 3) {
      return tugasTiga();
  } else if(pilihan == 4) {
      return tugasEmpat();
  } else if(pilihan == 5) {
      return tugasLima();
  } else {
      printf("Salah Memasukkan Pilihan \n");
  }
}

void main() {
   pilihanTugas();
}