#include <stdio.h>

int tugas1() {
  char str1[100] = "Hanafi";
  char str2[100];
  for (char *p1 = str1, *p2 = str2; *p1 != '\0'; p1++, p2++) {
    *p2 = *p1;
  }
  char  *p2 = '\0';
  printf("String str2: %s\n", str2);

  return 0;
}



int tugas2() {
  char str1[100] = "Hello, ";
  char str2[100] = "world!";
  char str3[200];
  for (char *p1 = str1, *p3 = str3; *p1 != '\0'; p1++, p3++) {
    *p3 = *p1;
  }
  for (char *p2 = str2, *p3 = p3; *p2 != '\0'; p2++, p3++) {
    *p3 = *p2;
  }
  char *p3 = '\0';
  printf("String str3: %s\n", str3);

  return 0;
}
