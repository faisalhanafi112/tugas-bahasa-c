#include <stdio.h>

void Tugas1() {
   char s[] = "TEKNIK KOMPUTER";
   char *ps;

   ps = s;
   printf("karakter ke -1 %c \n", *ps);
   printf("karakter ke -2 %c \n", *(ps+1));
}


void Tugas2() {
   int data[4];
   int *pi;

   pi = data;
   *pi = 2;
   *(pi+1) = 6;
   *(pi+2) = 9;
   *(pi+3) = 11;
}

void main() {
  Tugas2();
}
