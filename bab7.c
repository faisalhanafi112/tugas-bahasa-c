// Nama : Faisal Hanafi
// Prodi : D3 Teknik Komputer
// NIM : 131221018

#include <stdio.h>

void Tugas1()
{
    int nilai[10] = {1,2,3,4,5,6,7,8,9,10};
    int loop;

    for(loop = 0; loop < 10; loop++)
      printf("%d ", nilai[loop]);
}


void Tugas2()
{
    char huruf[5] = {'a', 'i', 'u', 'e', 'o'};
    int loop;

    for(loop = 0; loop < 5; loop++)
      printf("%c ", huruf[loop]);
}


void Tugas3()
{
    float pecahan[5] = {1.2, 3.4, 5.6, 7.8, 9.0};
    int loop;

    for(loop = 0; loop < 5; loop++)
      printf("%.1f ", pecahan[loop]);
}


void Tugas4()
{
    int nay[5][2] = {{1,70},{2,85},{3,90},{4,75},{5,85}};
    int i,j;
    for(i = 0; i < 5; i++) {
        for(j = 0; j < 2; j++) {
            printf("%d", nay[i][j]);
        }
        printf("\n");
    }
}


void Tugas5()
{
    int nilai[10];
    int i,a;
    for (int i = 0; i < 10; i++) {
         printf("masukan nilai :");
         scanf("%i",&a);
         nilai[i] = a;
  }
        printf("array ke 1 %i \n", nilai[0]);
        printf("array ke 2 %i \n", nilai[1]);
        printf("array ke 3 %i \n", nilai[2]);
        printf("array ke 4 %i \n", nilai[3]);
        printf("array ke 5 %i \n", nilai[4]);
        printf("array ke 6 %i \n", nilai[5]);
        printf("array ke 7 %i \n", nilai[6]);
        printf("array ke 8 %i \n", nilai[7]);
        printf("array ke 9 %i \n", nilai[8]);
        printf("array ke 10 %i \n", nilai[9]);
}


void Tugas6() {
    int nilai[20] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
    int i;

    for (int i = 0; i < 20; i++) {
         if(nilai[i] % 2 != 0) {
             printf("nilai ganjilnya %i \n",nilai[i]);
         }
  }
}


void Tugas7()
{
    char ind[9] = {'i','n','d','o','n','e','s','i','a'};
    printf("%s",ind);
}

void Tugas8()
{
    char ind[9] = {'i','n','d','o','n','e','s','i','a'};
    char isi;
    printf("masukan karakter yang akan diterlusuri : ");
    scanf("%c", &isi);
    switch(isi) {
        case 'i':
            printf("ingat");
            break;
        case 'n' :
            printf("nano-nano");
            break;
        case 'd' :
            printf("dominan");
            break;
        case 'o':
            printf("omong-omong");
            break;
        case 'e':
            printf("enak");
            break;
        case 's':
            printf("salam");
            break;
        case 'a':
            printf("ada");
            break;
        default:
            printf("Maaf, format karakter tidak sesuai \n");
    }
}


 void main()
 {
     //Tugas1();
     //Tugas2();
     //Tugas3();
     //Tugas4();
     //Tugas5();
     //Tugas6();
     //Tugas7();
     Tugas8();
 }
