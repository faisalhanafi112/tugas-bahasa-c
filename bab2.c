// Nama : Faisal Hanafi
// Prodi : D3 Teknik Komputer
// NIM : 131221018

#include <stdio.h>
#define PHI 3.14
#define MAXSIZE 255
#define ROOTDIR "C"
#define MIN_KELVIN 80.0
#define R 8.314472

void integer() 
{
    int jumlah,nilai_hexa,nilai_oktal;
    //int
    jumlah = 0;
    nilai_hexa = 0x1A;
    nilai_oktal = 022;
    printf("\n === Integer === \n");   
    printf("jumlah : %d \n",jumlah);
    printf("hexa : %d \n",nilai_hexa);
    printf("oktal : %d \n",nilai_oktal);
}


void dobel() 
{
    double suhu_awal,harga;
    // double
    suhu_awal = 25.0;
    harga = 10000.00;
    printf("\n === Double === \n");
    printf("suhu awal : %.1lf \n",suhu_awal);
    printf("harga : %.2lf \n",harga);
}

void floate() 
{
    float radius;
    // float
    radius = 0.0;
    printf("\n === Float === \n");
    printf("radius : %.1f \n",radius);
}

void karakter() 
{
    char jawaban;
    // char
    jawaban = 'B';
    printf("\n === Karakter === \n");
    printf("jawaban : %c \n\n",jawaban);
}

void main() {
    integer();
    dobel();
    floate();
    karakter();
    printf("=== Konstanta ===\n");
    printf("PHI = %.2f \n", PHI);
    printf("MAXSIZE = %d \n", MAXSIZE);
    printf("ROOTDIR = %s \n", ROOTDIR);
    printf("MIN.KELVIN = %.1f \n", MIN_KELVIN);
    printf("R = %f \n", R);
}