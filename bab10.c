//#include <stdio.h>
//tugas 1 & 2
//int cariMin(int a, int b) {
//  return (a < b) ? a : b;
//}
//int main() {
//  int a, b;

//  printf("Masukkan angka pertama: ");
//  scanf("%d", &a);
//  printf("Masukkan angka kedua: ");
//  scanf("%d", &b);

//  printf("Nilai terkecil diantara %d dan %d adalah %d\n", a, b, cariMin(a, b));

//  return 0;
//}


//tugas3 dan tugas 4

//#include <stdio.h>
//#include <math.h>
//double volumeKerucut(double r, double t) {
//  return (M_PI * r * r * t) / 3.0;
//}

//int main() {
//  double r, t;

//  printf("Masukkan nilai jari-jari: ");
//  scanf("%lf", &r);
//  printf("Masukkan nilai tinggi: ");
//  scanf("%lf", &t);

//  printf("Volume kerucut dengan jari-jari %.2lf dan tinggi %.2lf adalah %.2lf\n", r, t, volumeKerucut(r, t));

//  return 0;
//}



//tugas 5
#include <stdio.h>
int apakahGanjil(int x) {
  return (x % 2 == 1);
}

int main() {
  int x;

  printf("Masukkan sebuah angka: ");
  scanf("%d", &x);

  if (apakahGanjil(x)) {
    printf("Angka %d termasuk angka ganjil\n", x);
  } else {
    printf("Angka %d bukan termasuk angka ganjil\n", x);
  }

  return 0;
}

